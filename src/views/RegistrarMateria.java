/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Don Comedia
 */
public class RegistrarMateria  extends JFrame{
    
   public RegistrarMateria() {
    inicializar();
    dimensionar();
    adicionar();
    visualizar();
    accionar();

    }
   
  private JLabel parteAbajo;
    private JLabel parteArriba;
    private JLabel etq1;
    private JLabel etq2;
    private JTextField jt1;
    private JButton jb1;
   
    public void inicializar() {
        this.setLayout(null);

      
        this.etq1= new JLabel("Registrar materia");
        this.etq2= new JLabel("Nombre");
        this.jt1 = new JTextField();
        this.jb1 = new JButton("Registrar");
         this.parteAbajo = new JLabel();
              this.parteAbajo.setIcon(new ImageIcon("src/img/parteAbajoTodo.png"));
         this.parteArriba = new JLabel();
        this.parteArriba.setIcon(new ImageIcon("src/img/parteArribaTodo.png"));
        
    }
    public void dimensionar() {
      this.parteAbajo.setBounds(0,768,1344,59);
        this.parteArriba.setBounds(0,0,1344,59);
        this.etq1.setBounds(600, 200, 175, 100);
        this.etq2.setBounds(600, 230, 175, 100);
        this.jt1.setBounds(650, 270, 175, 25);
        this.jb1.setBounds(650, 300, 175, 25);
    }
    
    public void adicionar() {
        this.add(this.parteArriba);
	this.add(this.parteAbajo);
        this.add(this.etq1);
        this.add(this.jt1);
        this.add(this.jb1);
        this.add(this.etq2);
    }
    public void visualizar() {
          this.setSize(1344, 865);
        this.setVisible(true);
        
        this.getContentPane().setBackground(Color.WHITE);

    }
    
    
    
              	public void accionar() {
		this.jb1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String nombreMateria = "";
                                nombreMateria = jt1.getText();
                                System.out.println("Se registra la materia: " + nombreMateria);
                                
                                
                      
			}
		});
	}
    
}
